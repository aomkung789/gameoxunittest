/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import Test.Player;

/**
 *
 * @author OM
 */
public class PlayerTest {

    public PlayerTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    public void testPlayerO() {
        Player test = new Player('o');
        test.getName();
        assertTrue(test.getName() == 'o');
    }

    public void testPlayerX() {
        Player test = new Player('x');
        test.getName();
        assertTrue(test.getName() == 'x');
    }

    public void testPlayerOfail() {
        Player test = new Player('o');
        test.getName();
        assertFalse(test.getName() == 'x');

    }

    public void testPlayerXfail() {
        Player test = new Player('x');
        test.getName();
        assertFalse(test.getName() == 'o');
    }

    public void testXWinCount() {
        Player win = new Player('x');
        win.getWin();
        assertFalse(win.getWin() == +1);

    }
    

    public void testOWinCount() {
        Player win = new Player('o');
        win.getWin();
        assertFalse(win.getWin() == +1);

    }

    public void testPlayerDrawCount() {
        Player draw = new Player('x');
        draw.getDraw();
        assertFalse(draw.getDraw() == +1);

    }


    public void testOLoseCount() {
        Player lose = new Player('o');
        lose.getLose();
        assertFalse(lose.getLose() == +1);

    }

    public void testXLoseCount() {
        Player lose = new Player('x');
        lose.getLose();
        assertFalse(lose.getLose() == +1);

    }
    public void testXWinCountfail() {
        Player win = new Player('x');
        win.getWin();
        assertFalse(win.getWin() == +2);

    }
     public void testOWinCountfail() {
        Player win = new Player('o');
        win.getWin();
        assertFalse(win.getWin() == +12);

    }

    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
